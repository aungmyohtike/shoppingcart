<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
	<title>Shopping Cart</title>
</head>
<body>
<h1>Your Shopping Cart</h1>
<c:forEach items="${cart}" var="product">
  Name: ${product.name}, 
  price: <fmt:formatNumber value="${product.price}" maxFractionDigits="2"/> <br/>
</c:forEach>

<h1>Add Product</h1>
<form method="post" action="addProduct">
  Name: <input type="text" name="name"/>
  Price: <input type="text" name="price"/>
  <button>Add Product</button>
</form>
</body>
</html>
