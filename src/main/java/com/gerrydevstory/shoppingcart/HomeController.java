package com.gerrydevstory.shoppingcart;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("/")
@SessionAttributes({"cart"})
public class HomeController {

  @RequestMapping(method = RequestMethod.GET)
  public String get(Model model) {
    if(!model.containsAttribute("cart")) {
      model.addAttribute("cart", new ArrayList<Product>());
    }
    return "home";
  }
  
  @RequestMapping(value = "addProduct", method = RequestMethod.POST)
  public String addProduct(@ModelAttribute Product product,
      @ModelAttribute("cart") List<Product> cart) {
    cart.add(product);
    return "redirect:/";
  }

}
